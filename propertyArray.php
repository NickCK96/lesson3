<?php
$propertyArray = [
    [
        'type' => 'hotel_room',
        'address' =>'Hamilton Road',
        'price' => 1500,
        'description' => 'comfortable room with big window',
        'roomNumber' => 1,
    ],[
        'type' => 'apartment',
        'address' =>'Horton Street',
        'price' => 860,
        'description' => 'comfortable room with big hall',
        'kitchen' => true,
    ],[
        'type' => 'house',
        'address' =>'Springbank Drive',
        'price' => 2500,
        'description' => 'comfortable room with pool',
        'roomsAmount' => 5,
    ],[
        'type' => 'hotel_room',
        'address' =>'Hamilton Road',
        'price' => 1500,
        'description' => 'comfortable room with big window',
        'roomNumber' => 1,
    ],[
        'type' => 'apartment',
        'address' =>'Horton Street',
        'price' => 860,
        'description' => 'comfortable room with big hall',
        'kitchen' => true,
    ],[
        'type' => 'house',
        'address' =>'Springbank Drive',
        'price' => 2500,
        'description' => 'comfortable room with pool',
        'roomsAmount' => 5,
    ],[
        'type' => 'hotel_room',
        'address' =>'Riverside Drive',
        'price' => 3500,
        'description' => 'comfortable room with big window',
        'roomNumber' => 8,
    ],[
        'type' => 'apartment',
        'address' =>'York Street',
        'price' => 160,
        'description' => 'comfortable room with hall',
        'kitchen' => false,
    ],[
        'type' => 'house',
        'address' =>'Springfild Drive',
        'price' => 2300,
        'description' => 'comfortable room with pool',
        'roomsAmount' => 2,
    ],[
        'type' => 'house',
        'address' =>'Western Road',
        'price' => 3000,
        'description' => 'comfortable room with big poo',
        'roomsAmount' => 6,
    ]
];
