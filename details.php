<?php
require_once 'propertyArray.php';
require_once 'classes/HotelRoom.php';
require_once  'classes/Apartment.php';
require_once  'classes/House.php';
require_once 'classes/HtmlWriter.php';

$id = $_GET['id'];
$propertyObj = null;
    switch ($propertyArray[$id]['type']){
            case "hotel_room":
                $propertyObj = new HotelRoom($propertyArray[$id]['address'],$propertyArray[$id]['price'],
                    $propertyArray[$id]['description'],$propertyArray[$id]['roomNumber']);
                break;
        case 'apartment':
            $propertyObj = new Apartment($propertyArray[$id]['address'],$propertyArray[$id]['price'],
                $propertyArray[$id]['description'],$propertyArray[$id]['kitchen']);
            break;
        case 'house':
            $propertyObj = new House($propertyArray[$id]['address'],$propertyArray[$id]['price'],
                $propertyArray[$id]['description'],$propertyArray[$id]['roomsAmount']);
            break;
    }

$htmlWriter = new HtmlWriter();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php switch ($propertyArray[$id]['type']):
        case "hotel_room":?>
        <?= $htmlWriter->writeHtmlHotelRoom($propertyObj);?>
        <?php break;?>
        <?php case "apartment":?>
            <?= $htmlWriter->writeHtmlApartment($propertyObj);?>
            <?php break;?><?php case "house":?>
            <?= $htmlWriter->writeHtmlHouse($propertyObj);?>
            <?php break;?>




        <?php endswitch; ?>
</body>
</html>
