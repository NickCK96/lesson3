<?php
require_once 'propertyArray.php';
require_once 'classes/HotelRoom.php';
require_once 'classes/Apartment.php';
require_once 'classes/House.php';
require_once 'details.php';

$propertyObjs = [];
foreach ($propertyArray as $key => $propertyElement){
    switch ($propertyElement['type']){
        case "hotel_room":
           $propertyObjs[] = new HotelRoom($propertyElement['address'],$propertyElement['price'],
               $propertyElement['description'],$propertyElement['roomNumber']);
            break;
        case 'apartment':
            $propertyObjs[] = new Apartment($propertyElement['address'],$propertyElement['price'],
                $propertyElement['description'],$propertyElement['kitchen']);
            break;
        case 'house':
            $propertyObjs[] = new House($propertyElement['address'],$propertyElement['price'],
                $propertyElement['description'],$propertyElement['roomsAmount']);
            break;
    }

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php foreach ($propertyObjs as $key => $propertyObj):?>
            <?=$propertyObj->getSummaryLine()?>
            <a href="details.php? id=<?=$key?>">Details</a>
        <?php endforeach;?>
</body>
</html>
